package co.uk.dmp9software.dmpdropparty;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class DropParty extends JavaPlugin implements Listener{
	public int secondsElapsed = 0;
	public boolean started = false;
	public DropPartyTimer thingaa = new DropPartyTimer();
	public void onEnable()
	{
		getLogger().info("Enabling DMP DropParty...");
		if (getConfig().getInt("locationtotal") != 0 && getConfig().contains("locations"))
		{
			getLogger().info("Starting DropPartyTimer...");
			Timer t = new Timer();
			t.schedule(thingaa, 1000);
			getLogger().info("Started!");
		}
		else
		{
			getLogger().warning("No locations detected. Have you created any?");
		}
		getLogger().info("Registering events...");
		getServer().getPluginManager().registerEvents(this,this);
	}
	
	public void onDisable()
	{
		getLogger().info("Disabling DMP DropParty...");
		saveConfig();
		thingaa.cancel();
		getLogger().info("Thank you and goodbye! :)");
	}
	
	class DropPartyTimer extends TimerTask
	{
		public void run() {
			tick();
		}
		
	}
	
	public List<Item> itemsDroppedByThis = null;
	
	public void tick()
	{
		secondsElapsed++;
		int duration = getConfig().getInt("duration");
		int waitTime = getConfig().getInt("wait-time");
		if (started && secondsElapsed == duration)
		{
			for (Player player : getServer().getOnlinePlayers())
			{
				player.sendMessage(getConfig().getString("finished-message"));
			}
			for (Item i : itemsDroppedByThis)
			{
				itemsDroppedByThis.remove(i);
			}
			started = false;
			secondsElapsed = 0;
		}
		else if (started && secondsElapsed != duration)
		{
			int i = getConfig().getInt("locationtotal");
			while (i != 0)
			{
				double x = getConfig().getDoubleList("locations.l" + i + ".coord").get(0);
				double y = getConfig().getDoubleList("locations.l" + i + ".coord").get(1);
				double z = getConfig().getDoubleList("locations.l" + i + ".coord").get(2);
				String world = getConfig().getString("locations.l" + i + ".world");
				Random r = new Random();
				Item item = getServer().getWorld(world).dropItem(new Location(getServer().getWorld(world),x,y,z), new ItemStack(Material.getMaterial(getConfig().getStringList("drop").get(r.nextInt(getConfig().getInt("droptotal"))))));
				itemsDroppedByThis.add(item);
				i--;
			}
		}
		else if (!started && secondsElapsed == getConfig().getInt("wait-time"))
		{
			for (Player player : getServer().getOnlinePlayers())
			{
				player.sendMessage(getConfig().getString("start-message"));
			}
			started = true;
			secondsElapsed = 0;
		}
		else if (!started && secondsElapsed != getConfig().getInt("wait-time"))
		{
			if (getConfig().getIntegerList("warnings").contains(secondsElapsed))
			{
				for (Player player : getServer().getOnlinePlayers())
				{
					if (getConfig().getString("warning-message").contains("(ARGMINS)")){player.sendMessage(getConfig().getString("warning-message").replaceAll("(ARGMINS)", "" + getConfig().getInt("wait-time") * 60));}else{player.sendMessage(getConfig().getString("warning-message").replaceAll("(ARGSECONDS)", "" + getConfig().getInt("wait-time")));}
				}
			}
		}
		
	}
	
	@EventHandler
	public void onPlayerPickUpItem(PlayerPickupItemEvent event)
	{
		if (started && itemsDroppedByThis.contains(event.getItem()))
		{
			event.getPlayer().sendMessage(ChatColor.BOLD + "" + ChatColor.MAGIC + "kkk"+ChatColor.RESET+"" + ChatColor.AQUA + ""+ChatColor.BOLD+" You collected " + event.getItem().getType().getName() + "! " + ChatColor.BOLD + "" + ChatColor.MAGIC + "kkk");
			event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.NOTE_PLING, 1F, 0);
		}
	}
	
	@SuppressWarnings("null")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		Player p = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("dpaddlocation"))
		{
			int locationcount = getConfig().getInt("locationtotal");
			if (locationcount == 0){
				List<Double> thing = null;
				String anotherthing = "";
				thing.add(0,p.getLocation().getX());
				thing.add(1,p.getLocation().getY());
				thing.add(2,p.getLocation().getZ());
				anotherthing = p.getLocation().getWorld().getName();
				getConfig().set("locations.l" + locationcount++ + ".coord",thing);
				getConfig().set("locations.l" + locationcount + ".world",anotherthing);
				getConfig().set("locationtotal",locationcount);
				Timer t = new Timer();
				t.schedule(thingaa, 1000);
				saveConfig();
				p.sendMessage("Congratulations! You created your first location! The drop party will begin in " + getConfig().getInt("wait-time") + " seconds.");
				p.sendMessage("Meanwhile you can change other thingsabout the drop party:");
				p.sendMessage("/dpsetduration <x> - Sets the duration of the drop party to x seconds");
				p.sendMessage("/dpsetwaittime <x> - Sets the time between each drop party to x seconds");
				p.sendMessage("/dpadditem <item> - Adds an item that will be dropped ");
				p.sendMessage(" ");
				p.sendMessage("And hey! Why not make more locations? :)");
				return true;
			}
			else
			{
				List<Double> thing = null;
				String anotherthing = "";
				thing.add(0,p.getLocation().getX());
				thing.add(1,p.getLocation().getY());
				thing.add(2,p.getLocation().getZ());
				anotherthing = p.getLocation().getWorld().getName();
				getConfig().set("locations.l"+locationcount+++".coord",thing);
				getConfig().set("locations.l" + locationcount + ".world",anotherthing);
				getConfig().set("locationtotal",locationcount);
				saveConfig();
				p.sendMessage("Set");
				return true;
			}
		}
		else if (cmd.getName().equalsIgnoreCase("dpstart"))
		{
			
		}
		else if (cmd.getName().equalsIgnoreCase("dpsetduration"))
		{
			if (args.length == 0){p.sendMessage("Valid usage: /dpsetduration <item>");return true;}
			getConfig().set("duration", args[0]);
			saveConfig();
			p.sendMessage("Set");
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("dpsetwaittime"))
		{
			if (args.length == 0){p.sendMessage("Valid usage: /dpsetwaittime <item>");return true;}
			getConfig().set("wait-time", args[0]);
			saveConfig();
			p.sendMessage("Set");
			return true;
		}
		else if (cmd.getName().equalsIgnoreCase("dpadditem"))
		{
			getLogger().info("DMP DropParty is scanning drops to check if " + args[0] + " is already set...");
			for (String s : getConfig().getStringList("drops"))
			{
				getLogger().info("Found drop " + s);
				if (args[0].equalsIgnoreCase(s)){getLogger().warning(args[0]+" is equal to "+s);p.sendMessage("That drop is already in the list!");return true;}else{getLogger().info(args[0]+" is not equal to "+s);}
			}
			getConfig().set("drops", getConfig().getStringList("drops").add(args[0]));
			p.sendMessage("Set");
			return true;
		}
		return false;
	}
}
